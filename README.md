# Tanuki Snooze

## Installation

* Clone repository
* Go to `chrome://extensions`
* Toggle `Developer Mode` to on
* Click `Load unpacked`
* Select repository directory

## Usage

Visit your todos page and a `Snooze` button shoud be next to each `Done` button

Snoozing a todo is configurable based on how many hours you wish to snooze the to-do.

You can clear all snoozes (aka: reset back to normal) by selecting the Tanuki Snoozer icon in the extensions icon and clicking the `Unsnooze All!` button.

